{
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "http://example.com/example.json",
    "type": "object",
    "title": "The Root Schema",
    "description": "The root schema comprises the entire JSON document.",
    "required": [
        "configure",
        "ingest",
        "process",
        "stage"
    ],
    "properties": {
        "configure": {
            "$id": "#/properties/configure",
            "type": "object",
            "title": "The Configure Schema",
            "description": "Configure the pipeline",
            "default": {},
            "examples": [
                {
                    "execution_engine": "rascil",
                    "memory": null,
                    "project": "eMERGE_DR1",
                    "distributed": true,
                    "nworkers": null
                }
            ],
            "required": [
                "project",
                "execution_engine",
                "distributed",
                "nworkers",
                "memory"
            ],
            "properties": {
                "project": {
                    "$id": "#/properties/configure/properties/project",
                    "type": "string",
                    "title": "The Project Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "eMERGE_DR1"
                    ]
                },
                "execution_engine": {
                    "$id": "#/properties/configure/properties/execution_engine",
                    "type": "string",
                    "title": "The Execution_engine Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "rascil"
                    ]
                },
                "distributed": {
                    "$id": "#/properties/configure/properties/distributed",
                    "type": "boolean",
                    "title": "The Distributed Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": false,
                    "examples": [
                        true
                    ]
                },
                "nworkers": {
                    "$id": "#/properties/configure/properties/nworkers",
                    "title": "The Nworkers Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": null,
                    "examples": [
                        null
                    ]
                },
                "memory": {
                    "$id": "#/properties/configure/properties/memory",
                    "title": "The Memory Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": null,
                    "examples": [
                        null
                    ]
                }
            }
        },
        "ingest": {
            "$id": "#/properties/ingest",
            "type": "object",
            "title": "The Ingest Schema",
            "description": "An explanation about the purpose of this instance.",
            "default": {},
            "examples": [
                {
                    "flag_strategy": "eMERLIN_strategy.rfis",
                    "original_nchan": 128.0,
                    "nchan": 128.0,
                    "source": "1236+621",
                    "verbose": false,
                    "data_column": "DATA",
                    "ms_path": "/mnt/storage-ssd/tim/workspace/eMERGE_DR1/data/emerge_dr1_processed_17JUL2015.ms",
                    "dds": [
                        1.0,
                        3.0,
                        5.0,
                        7.0,
                        9.0,
                        11.0,
                        13.0,
                        15.0
                    ]
                }
            ],
            "required": [
                "verbose",
                "ms_path",
                "data_column",
                "source",
                "dds",
                "flag_strategy",
                "original_nchan",
                "nchan"
            ],
            "properties": {
                "verbose": {
                    "$id": "#/properties/ingest/properties/verbose",
                    "type": "boolean",
                    "title": "The Verbose Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": false,
                    "examples": [
                        false
                    ]
                },
                "ms_path": {
                    "$id": "#/properties/ingest/properties/ms_path",
                    "type": "string",
                    "title": "The Ms_path Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "/mnt/storage-ssd/tim/workspace/eMERGE_DR1/data/emerge_dr1_processed_17JUL2015.ms"
                    ]
                },
                "data_column": {
                    "$id": "#/properties/ingest/properties/data_column",
                    "type": "string",
                    "title": "The Data_column Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "DATA"
                    ]
                },
                "source": {
                    "$id": "#/properties/ingest/properties/source",
                    "type": "string",
                    "title": "The Source Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "1236+621"
                    ]
                },
                "dds": {
                    "$id": "#/properties/ingest/properties/dds",
                    "type": "array",
                    "title": "The Dds Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": [],
                    "items": {
                        "$id": "#/properties/ingest/properties/dds/items",
                        "type": "integer",
                        "title": "The Items Schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": 0,
                        "examples": [
                            1,
                            3,
                            5,
                            7,
                            9,
                            11,
                            13,
                            15
                        ]
                    }
                },
                "flag_strategy": {
                    "$id": "#/properties/ingest/properties/flag_strategy",
                    "type": "string",
                    "title": "The Flag_strategy Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "eMERLIN_strategy.rfis"
                    ]
                },
                "original_nchan": {
                    "$id": "#/properties/ingest/properties/original_nchan",
                    "type": "integer",
                    "title": "The Original_nchan Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        128
                    ]
                },
                "nchan": {
                    "$id": "#/properties/ingest/properties/nchan",
                    "type": "integer",
                    "title": "The Nchan Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        128
                    ]
                }
            }
        },
        "process": {
            "$id": "#/properties/process",
            "type": "object",
            "title": "The Process Schema",
            "description": "An explanation about the purpose of this instance.",
            "default": {},
            "examples": [
                {
                    "calibration_context": "TG",
                    "niter": 1000.0,
                    "stokes": "I",
                    "scales": [
                        0.0,
                        3.0,
                        10.0
                    ],
                    "npixel": 8192.0,
                    "restore_facets": 8.0,
                    "B_first_selfcal": 8.0,
                    "window_shape": "no_edge",
                    "nmoment": 2.0,
                    "do_selfcal": false,
                    "deconvolve_facets": 8.0,
                    "fractional_threshold": 0.3,
                    "global_solution": true,
                    "G_first_selfcal": 5.0,
                    "robustness": 0.0,
                    "B_timeslice": 100000.0,
                    "T_timeslice": 0.0,
                    "cellsize": 9e-08,
                    "tol": 1e-08,
                    "B_phase_only": false,
                    "imaging_context": "ng",
                    "verbose": false,
                    "algorithm": "mmclean",
                    "nmajor": 1.0,
                    "weighting_algorithm": "uniform",
                    "T_phase_only": true,
                    "T_first_selfcal": 2.0,
                    "do_wstacking": false,
                    "gain": 0.1,
                    "G_phase_only": false,
                    "G_timeslice": 1200.0,
                    "threshold": 0.003
                }
            ],
            "required": [
                "verbose",
                "weighting_algorithm",
                "robustness",
                "npixel",
                "cellsize",
                "stokes",
                "imaging_context",
                "do_wstacking",
                "nmajor",
                "niter",
                "algorithm",
                "nmoment",
                "gain",
                "scales",
                "threshold",
                "fractional_threshold",
                "window_shape",
                "imaging_facets",
                "deconvolve_facets",
                "restore_facets",
                "T_first_selfcal",
                "T_phase_only",
                "T_timeslice",
                "G_first_selfcal",
                "G_phase_only",
                "G_timeslice",
                "B_first_selfcal",
                "B_phase_only",
                "B_timeslice",
                "global_solution",
                "calibration_context",
                "do_selfcal",
                "tol"
            ],
            "properties": {
                "verbose": {
                    "$id": "#/properties/process/properties/verbose",
                    "type": "boolean",
                    "title": "The Verbose Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": false,
                    "examples": [
                        false
                    ]
                },
                "weighting_algorithm": {
                    "$id": "#/properties/process/properties/weighting_algorithm",
                    "type": "string",
                    "title": "The Weighting_algorithm Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "uniform"
                    ]
                },
                "robustness": {
                    "$id": "#/properties/process/properties/robustness",
                    "type": "number",
                    "title": "The Robustness Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        0
                    ]
                },
                "npixel": {
                    "$id": "#/properties/process/properties/npixel",
                    "type": "integer",
                    "title": "The Npixel Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        8192
                    ]
                },
                "cellsize": {
                    "$id": "#/properties/process/properties/cellsize",
                    "type": "number",
                    "title": "The Cellsize Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        9e-08
                    ]
                },
                "stokes": {
                    "$id": "#/properties/process/properties/stokes",
                    "type": "string",
                    "title": "The Stokes Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "I"
                    ]
                },
                "imaging_context": {
                    "$id": "#/properties/process/properties/imaging_context",
                    "type": "string",
                    "title": "The Imaging_context Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "ng"
                    ]
                },
                "do_wstacking": {
                    "$id": "#/properties/process/properties/do_wstacking",
                    "type": "boolean",
                    "title": "The Do_wstacking Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": false,
                    "examples": [
                        false
                    ]
                },
                "nmajor": {
                    "$id": "#/properties/process/properties/nmajor",
                    "type": "integer",
                    "title": "The Nmajor Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        1
                    ]
                },
                "niter": {
                    "$id": "#/properties/process/properties/niter",
                    "type": "integer",
                    "title": "The Niter Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        1000
                    ]
                },
                "algorithm": {
                    "$id": "#/properties/process/properties/algorithm",
                    "type": "string",
                    "title": "The Algorithm Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "mmclean"
                    ]
                },
                "nmoment": {
                    "$id": "#/properties/process/properties/nmoment",
                    "type": "integer",
                    "title": "The Nmoment Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        2
                    ]
                },
                "gain": {
                    "$id": "#/properties/process/properties/gain",
                    "type": "number",
                    "title": "The Gain Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        0.1
                    ]
                },
                "scales": {
                    "$id": "#/properties/process/properties/scales",
                    "type": "array",
                    "title": "The Scales Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": [],
                    "items": {
                        "$id": "#/properties/process/properties/scales/items",
                        "type": "integer",
                        "title": "The Items Schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": 0,
                        "examples": [
                            0,
                            3,
                            10
                        ]
                    }
                },
                "threshold": {
                    "$id": "#/properties/process/properties/threshold",
                    "type": "number",
                    "title": "The Threshold Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        0.003
                    ]
                },
                "fractional_threshold": {
                    "$id": "#/properties/process/properties/fractional_threshold",
                    "type": "number",
                    "title": "The Fractional_threshold Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        0.3
                    ]
                },
                "window_shape": {
                    "$id": "#/properties/process/properties/window_shape",
                    "type": "string",
                    "title": "The Window_shape Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "no_edge"
                    ]
                },
                "imaging_facets": {
                    "$id": "#/properties/process/properties/imaging_facets",
                    "type": "integer",
                    "title": "The Imaging_facets Schema",
                    "description": "Number of facets in imaging.",
                    "default": 1,
                    "examples": [
                        8
                    ]
                },
                "deconvolve_facets": {
                    "$id": "#/properties/process/properties/deconvolve_facets",
                    "type": "integer",
                    "title": "The Deconvolve_facets Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        8
                    ]
                },
                "restore_facets": {
                    "$id": "#/properties/process/properties/restore_facets",
                    "type": "integer",
                    "title": "The Restore_facets Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        8
                    ]
                },
                "T_first_selfcal": {
                    "$id": "#/properties/process/properties/T_first_selfcal",
                    "type": "integer",
                    "title": "The T_first_selfcal Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        2
                    ]
                },
                "T_phase_only": {
                    "$id": "#/properties/process/properties/T_phase_only",
                    "type": "boolean",
                    "title": "The T_phase_only Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": false,
                    "examples": [
                        true
                    ]
                },
                "T_timeslice": {
                    "$id": "#/properties/process/properties/T_timeslice",
                    "type": "number",
                    "title": "The T_timeslice Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        60.0
                    ]
                },
                "G_first_selfcal": {
                    "$id": "#/properties/process/properties/G_first_selfcal",
                    "type": "integer",
                    "title": "The G_first_selfcal Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        5
                    ]
                },
                "G_phase_only": {
                    "$id": "#/properties/process/properties/G_phase_only",
                    "type": "boolean",
                    "title": "The G_phase_only Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": false,
                    "examples": [
                        false
                    ]
                },
                "G_timeslice": {
                    "$id": "#/properties/process/properties/G_timeslice",
                    "type": "number",
                    "title": "The G_timeslice Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        1200
                    ]
                },
                "B_first_selfcal": {
                    "$id": "#/properties/process/properties/B_first_selfcal",
                    "type": "integer",
                    "title": "The B_first_selfcal Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        8
                    ]
                },
                "B_phase_only": {
                    "$id": "#/properties/process/properties/B_phase_only",
                    "type": "boolean",
                    "title": "The B_phase_only Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": false,
                    "examples": [
                        false
                    ]
                },
                "B_timeslice": {
                    "$id": "#/properties/process/properties/B_timeslice",
                    "type": "number",
                    "title": "The B_timeslice Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        100000
                    ]
                },
                "global_solution": {
                    "$id": "#/properties/process/properties/global_solution",
                    "type": "boolean",
                    "title": "The Global_solution Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": false,
                    "examples": [
                        true
                    ]
                },
                "calibration_context": {
                    "$id": "#/properties/process/properties/calibration_context",
                    "type": "string",
                    "title": "The Calibration_context Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "TG"
                    ]
                },
                "do_selfcal": {
                    "$id": "#/properties/process/properties/do_selfcal",
                    "type": "boolean",
                    "title": "The Do_selfcal Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": false,
                    "examples": [
                        false
                    ]
                },
                "tol": {
                    "$id": "#/properties/process/properties/tol",
                    "type": "number",
                    "title": "The Tol Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        1e-08
                    ]
                }
            }
        },
        "stage": {
            "$id": "#/properties/stage",
            "type": "object",
            "title": "The Stage Schema",
            "description": "An explanation about the purpose of this instance.",
            "default": {},
            "examples": [
                {
                    "number_moments": 1.0,
                    "msout": "/mnt/storage-ssd/tim/workspace/eMERGE_DR1/data/emerge_dr1_processed_17JUL2015_processed.ms",
                    "write_moments": true,
                    "results_directory": "./",
                    "verbose": false
                }
            ],
            "required": [
                "verbose",
                "results_directory",
                "write_moments",
                "number_moments",
                "msout"
            ],
            "properties": {
                "verbose": {
                    "$id": "#/properties/stage/properties/verbose",
                    "type": "boolean",
                    "title": "The Verbose Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": false,
                    "examples": [
                        false
                    ]
                },
                "results_directory": {
                    "$id": "#/properties/stage/properties/results_directory",
                    "type": "string",
                    "title": "The Results_directory Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "./"
                    ]
                },
                "write_moments": {
                    "$id": "#/properties/stage/properties/write_moments",
                    "type": "boolean",
                    "title": "The Write_moments Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": false,
                    "examples": [
                        true
                    ]
                },
                "number_moments": {
                    "$id": "#/properties/stage/properties/number_moments",
                    "type": "integer",
                    "title": "The Number_moments Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        1
                    ]
                },
                "msout": {
                    "$id": "#/properties/stage/properties/msout",
                    "type": "string",
                    "title": "The Msout Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": "",
                    "examples": [
                        "/mnt/storage-ssd/tim/workspace/eMERGE_DR1/data/emerge_dr1_processed_17JUL2015_processed.ms"
                    ]
                }
            }
        }
    }
}
