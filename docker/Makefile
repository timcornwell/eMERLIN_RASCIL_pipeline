build_all:
	cd erp;make build;
	cd erp-aoflagger; make build;

push_all:
	cd erp;make tag_latest;make push_latest
	cd erp-aoflagger;make tag_latest;make push_latest

push_all_stable:
	cd erp;make tag_stable;make push_stable
	cd erp-aoflagger;make tag_stable;make push_stable

build_and_push_all:
	cd erp;make build;make tag_latest;make push_latest
	cd erp-aoflagger; make build;make tag_latest;make push_latest

rm_all:
	cd erp;make rm
	cd erp-aoflagger; make rm

ls_all:
	cd erp;make ls
	cd erp-aoflagger; make ls

WORKDIR=./

IMAGES=timcornwell

# Docker tests
test_base:
	- echo "Running Dask-enabled test cluster_test_ritoy - takes about 20 -- 30 seconds"
	echo "cd ${WORKDIR};python3 /rascil/cluster_tests/ritoy/cluster_test_ritoy.py" | docker run -i  \
	${IMAGES}/erp /bin/bash -e

test_full:
	- echo "Running imaging.py - takes about 20 -- 30 seconds, writes three fits files in ${WORKDIR}"
	echo "cd ${WORKDIR};python3 /rascil/examples/scripts/imaging.py" | docker run -i --volume ${HOME}:${HOME} \
	${IMAGES}/erp-aoflagger /bin/bash -e

test_notebook:
	- echo "Running imaging notebook - takes about 20 -- 30 seconds, writes html file ${WORKDIR}/imaging.html"
	echo "cd ${WORKDIR};cp /rascil/examples/notebooks/imaging.ipynb .;jupyter nbconvert --execute --to html \
	imaging.ipynb" | docker run -i --volume /tmp:/tmp ${IMAGES}/rascil-notebook /bin/bash -e

# Singularity tests
test_base_singularity:
	- echo "Running Dask-enabled test cluster_test_ritoy - takes about 20 -- 30 seconds"
	- cd ${WORKDIR};singularity pull erp.img docker://${IMAGES}/erp
	cd ${WORKDIR};singularity exec erp.img python3 /rascil/cluster_tests/ritoy/cluster_test_ritoy.py

test_full_singularity:
	- echo "Running imaging.py - takes about 20 -- 30 seconds, writes three fits files in ${WORKDIR}"
	- cd ${WORKDIR};singularity pull erp-aoflagger.img docker://${IMAGES}/erp-aoflagger
	cd ${WORKDIR};singularity exec erp-aoflagger.img python3 /rascil/examples/scripts/imaging.py

test_notebook_singularity:
	- echo "Running imaging notebook - takes about 20 -- 30 seconds, writes html file ${WORKDIR}/imaging.html"
	- cd ${WORKDIR};singularity pull RASCIL-notebook.img docker://${IMAGES}/rascil-notebook
	cd ${WORKDIR};cp /rascil/examples/notebooks/imaging.ipynb .
	cd ${WORKDIR};singularity exec RASCIL-notebook.img "jupyter nbconvert --execute --to html imaging.ipynb"

test_all: test_base test_full test_notebook

test_all_singularity: test_base_singularity test_full_singularity test_notebook_singularity

everything: rm_all all test_all

.phony: build_all rm_all test_all test_all test_base test_full test_notebook test_all_singularity \
	test_all_singularity test_base_singularity test_full_singularity test_notebook_singularity everything

