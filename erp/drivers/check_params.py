"""
This is a python script for running the eMERLIN RASCIL pipeline::

    python3 erp_script.py --params eMERLIN_3C277_1.json

"""

__all__ = ['cli_parser']

import logging
import argparse
import json
import time
from jsonschema import validate
from jsonschema.exceptions import ValidationError

from erp.functions.pipelines import run_erp
from erp.functions.erp_path import erp_path
from erp.drivers.driver_support import get_logger

current_version = "0.0.1"

def cli_parser():
    ''' Command line parser
    
    :return: Parser with command line arguments
    '''
    # Assign description to the help doc
    description = 'e-MERLIN RASCIL pipeline parameter checker Visit: https://github.com/timcornwell/eMERLIN_RASCIL_pipeline'
    usage = 'python check_params.py [--inputs erp_params.json]'
    parser = argparse.ArgumentParser(description=description, usage=usage)
    parser.add_argument('-p', '--params', dest='erp_params', help='JSON file containing parameters',
                        default='./erp_params.json')
    return parser.parse_args()


if __name__ == "__main__":
    
    args = cli_parser()
    erp_params_file = args.erp_params

    with open(erp_params_file) as json_file:
        erp_params = json.loads(json_file.read())

    schema_file = erp_path("data/erp_schema.json")
    
    with open(schema_file) as json_file:
        erp_schema = json.loads(json_file.read())
        
    try:
        validate(erp_params, erp_schema)
    except ValidationError as e:
        print(str(e).split('\n')[0])
        print(str(e).split('\n')[2])
        exit(1)
        
    print("File {} passes checks".format(erp_params_file))
    exit(0)
